# build-yocto

Container for building yocto images with support for Boot2QT.

Base: Ubuntu 18.04

## Steps to build and push image

```
sudo service docker start
sudo docker image build -t miike/build-yocto .
sudo docker push miike/build-yocto
```